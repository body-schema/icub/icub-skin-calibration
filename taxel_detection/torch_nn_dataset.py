import os
import pandas as pd
import torch
from torch.utils.data import Dataset
from torchvision.io import read_image

from torch.utils.data import DataLoader
import matplotlib.pyplot as plt

from torchvision.transforms import ToTensor

class CustomImageDataset(Dataset):
    def __init__(self, annotations_file, img_dir, transform=None, target_transform=None):
        self.img_labels = pd.read_csv(annotations_file)
        self.img_dir = img_dir
        self.transform = transform
        self.target_transform = target_transform

    def __len__(self):
        return len(self.img_labels)

    def __getitem__(self, idx):
        img_path = os.path.join(self.img_dir, self.img_labels.iloc[idx, 0])
        image = read_image(img_path)
        image=image.type(torch.float32)
        label = self.img_labels.iloc[idx, 1]
        if self.transform:
            image = self.transform(image)
        if self.target_transform:
            label = self.target_transform(label)
        return image, label

if __name__ == '__main__':
    
    myDataset=CustomImageDataset("dataset/taxel_labels5.csv", "dataset/taxel_dataset5/")
    dataloader=DataLoader(myDataset, batch_size=16, shuffle=True)
    
    # Display image and label.
    train_features, train_labels = next(iter(dataloader))
    print(f"Feature batch shape: {train_features.size()}")
    print(f"Labels batch shape: {train_labels.size()}")
    img = train_features[0][0].squeeze()
    label = train_labels[0]
    plt.imshow(img)
    plt.show()
    print(f"Label: {label}")
    """
    for i in range(800):
        img=read_image("dataset/taxel_dataset5/"+str(i)+".jpg")
        if(img.shape!=torch.Size([3, 16, 16])):print(i)
    """
