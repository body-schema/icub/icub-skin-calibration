"""Torch learning"""
import torch
import numpy as np
import matplotlib.pyplot as plt
from torch import nn
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
from sklearn.metrics import confusion_matrix
from torch_nn_dataset import CustomImageDataset
from torch_nn_classes import mlp, cnn1, cnn2, cnn3, cnn4, cnn5

def learning_curve(model, train_data, test_data, loss_fn, optim, epochs):
    """compute and plot a model's learning curve"""
    tr_errors=np.zeros((epochs))
    tst_errors=np.zeros((epochs))
    for e in range(epochs):
        running_loss=0
        for batch, (X,y) in enumerate(train_data):
            logits=model(X)
            loss=loss_fn(logits[:,0], y.type(torch.float))
            running_loss+=loss.item()
            optim.zero_grad()
            loss.backward()
            optimizer.step()
        tr_errors[e]=running_loss/len(train_data)
        running_loss=0

        for X,y in test_data:
            logits=model(X)
            loss=loss_fn(logits[:,0], y.type(torch.float))
            running_loss+=loss.item()
        tst_errors[e]=running_loss/len(test_data)
        
    return(tr_errors,tst_errors)

def conf_matrix(model,test_data):
    """compute a model's confusion matrix on validation dataset"""
    M=np.zeros((2,2))
    for X,y in test_data:
        logits=model(X)
        sig=nn.Sigmoid()
        pred_probab = sig(logits)
        y_pred = np.round(pred_probab.detach().numpy())
        bcm=confusion_matrix(y_pred,y)
        #print(bcm)
        M+=bcm
    return M
        

if __name__ == "__main__":
    model=cnn4()

    #load dataset
    """
    dataset1=CustomImageDataset("dataset/taxel_labels1.csv", "dataset/taxel_dataset1/")
    dataset2=CustomImageDataset("dataset/taxel_labels2.csv", "dataset/taxel_dataset2/")
    dataset3=CustomImageDataset("dataset/taxel_labels3.csv", "dataset/taxel_dataset3/")
    dataset=torch.utils.data.ConcatDataset([dataset1,dataset2,dataset3])
    """
    dataset4=CustomImageDataset("dataset/taxel_labels4.csv", "dataset/taxel_dataset4/")
    dataset5=CustomImageDataset("dataset/taxel_labels5.csv", "dataset/taxel_dataset5/")
    dataset=torch.utils.data.ConcatDataset([dataset4,dataset5])
    
    #split dataset into train and validation part
    indices= list(range(len(dataset)))
    split = int(np.floor(0.2 * len(dataset)))
    train_indices, val_indices = indices[split:], indices[:split]

    train_sampler = SubsetRandomSampler(train_indices)
    valid_sampler = SubsetRandomSampler(val_indices)

    train_loader = DataLoader(dataset, batch_size=32, 
                                               sampler=train_sampler)
    validation_loader = DataLoader(dataset, batch_size=32,
                                                    sampler=valid_sampler)
    
    loss_fn=torch.nn.BCEWithLogitsLoss()
    optimizer=torch.optim.SGD(model.parameters(), lr=0.001)
    tr_errors,tst_errors=learning_curve(model, train_loader,validation_loader, loss_fn, optimizer, 100)

    plt.figure()
    plt.plot(tr_errors)
    plt.plot(tst_errors)
    plt.legend(["train","validation"])
    plt.xlabel("Epoch")
    plt.ylabel("Loss")
    plt.show()

    #confusion matrix for validation dataset
    cf=conf_matrix(model,validation_loader)
    print(cf)
    print(tst_errors[-1])
    torch.save(model.state_dict(), 'weights/model_weights.pth')

