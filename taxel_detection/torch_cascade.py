"""torch cascade classifier"""
"""
layers:
1) depth filter
2) color filter
3) CNN
"""

import pyrealsense2 as rs
import numpy as np
import cv2
import time
import torch
import matplotlib.pyplot as plt
import torchvision.transforms.functional as F

from segmentation import create_segm_mask
from utils import transformation_matrix
from plt_click_event import closest_valid_point
from torch_nn_classes import mlp, cnn1, cnn4

def sliding_window(image, stepSize, windowSize):
    # slide a window across the image
    for y in range(0, image.shape[1], stepSize):
        for x in range(0, image.shape[2], stepSize):
            # yield the current window
            yield (x, y, image[:,y:y + windowSize[1], x:x + windowSize[0]])

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()

w,h=1280,720
config.enable_stream(rs.stream.depth, w, h, rs.format.z16, 15)
config.enable_stream(rs.stream.color, w, h, rs.format.bgr8, 15)

# take a snapshot
pipeline.start(config)

#align
align_to = rs.stream.color
align = rs.align(align_to)

#pointcloud
pc=rs.pointcloud()

time.sleep(2) #sleep to give the camera time to start
frames=pipeline.wait_for_frames()

aligned_frames = align.process(frames)

aligned_depth_frame = aligned_frames.get_depth_frame()
color_frame = aligned_frames.get_color_frame()

# depth filter
depth_filter=rs.threshold_filter(0.,0.5)
filtered_depth_frame=depth_filter.process(aligned_depth_frame)

# frames as array
depth_image = np.asanyarray(filtered_depth_frame.get_data())
color_image = np.asanyarray(color_frame.get_data())

"""
# load images from folder (the pointcloud making doesn't work then)
depth_image = np.load("cascade_test_D.npy")
color_image = np.load("cascade_test_RGB.npy")
"""

# frames as torch
depth_image_t=torch.from_numpy(depth_image.astype('int32'))
color_image_t=torch.from_numpy(color_image).permute(2,0,1)
color_image_t=color_image_t[[2,1,0],:] #BGR to RGB (or vice versa?)

(imHeight,imWidth)=(color_image.shape[0],color_image.shape[1])

# initialize detection mask
np_mask=np.where(depth_image > 0, 1, 0).astype('bool')

# depth filter
#change points behind the threshold to 0
depth_image_3d = np.dstack((depth_image,depth_image,depth_image))
filtered_color_image=np.where((depth_image_3d<500) & (depth_image_3d > 0), color_image, 0)

# color filter
#change points with uncorresponding color to 0
"""
np_mask2=np.zeros((imHeight,imWidth),dtype=np.uint8)
for (x,y,val) in create_segm_mask(filtered_color_image):
    np_mask2[x,y]=val

np_mask=np_mask & np_mask2
"""
t_mask=torch.from_numpy(np_mask)

#pipeline.stop()
print("snaphot made (you can put the camera away)")

# CNN sliding window
#slide through the points with value 1

model=cnn4()
model.load_state_dict(torch.load('weights/cnn4_weights45.pth'))
model.eval()

window_tensor=torch.zeros([1, 3, 16, 16])
detection_mask=torch.zeros([imHeight,imWidth],dtype=torch.bool)

for (x, y, window) in sliding_window(color_image_t, stepSize=1, windowSize=(16, 16)):
    if window.shape[1] != 16 or window.shape[2] != 16:
        continue
    if t_mask[y+8,x+8]==0:
        continue
    #detection_mask[y+8,x+8]=1 #delete, just for testing
    window_tensor[0,:,:,:]=window
    pred=model(window_tensor)
    sig=torch.nn.Sigmoid()
    pred_probab = sig(pred)
    y_pred = np.round(pred_probab.detach().numpy())
    if y_pred == 1:
        detection_mask[y+8,x+8]=1 #saves window's center to the mask

#masked=cv2.bitwise_and(color_image,color_image,mask=np_mask)

#contours
det_mask_np=detection_mask.numpy().astype(np.uint8)
contours, _ = cv2.findContours(det_mask_np,cv2.CHAIN_APPROX_NONE,cv2.RETR_LIST)
#find centroids of contours
centroids=np.zeros((len(contours),2))
n=0
for c in contours:
    x = [p[0][0] for p in c]
    y = [p[0][1] for p in c]
    centroid = (sum(x) / len(c), sum(y) / len(c))
    centroids[n,0]=centroid[0]
    centroids[n,1]=centroid[1]
    n+=1

# visualize image with result mask
"""
for x in range(np_mask.shape[0]):
    for y in range(np_mask.shape[1]):
        if np_mask[x,y]==1:
            cv2.circle(filtered_color_image,(y,x),radius=0,color=(0,255,0),thickness=-1)
"""

#make and save a pointcloud

pc.map_to(color_frame)
points = pc.calculate(filtered_depth_frame)
#points.export_to_ply('./cascade_pc_out.ply',color_frame)

np_points=np.asanyarray(points.get_vertices(2))
centroids3D=np.zeros((centroids.shape[0],3))
for k in range(centroids.shape[0]):
    centroids3D[k,:]=np_points[round(centroids[k,1])*w+round(centroids[k,0])]

#cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
#cv2.imshow('RealSense', filtered_color_image)

# visualize image with centroids
img=color_image_t.detach()
img=F.to_pil_image(img)

fig1=plt.figure("2D detection")
#plt.imshow(np_mask)
plt.imshow(np.asarray(img))
plt.scatter(centroids[:,0],centroids[:,1])

index=0
def onpick(event):
    global index
    chosen_coord_center_2D=np.zeros((2,))
    chosen_coord_center_2D=closest_valid_point(event.xdata,event.ydata,centroids)
    index=np.where(centroids==chosen_coord_center_2D)[0][0]

cid=fig1.canvas.mpl_connect('button_press_event', onpick)
print("Click on a triangle center.")
plt.show()
fig1.canvas.mpl_disconnect(cid)
chosen_coord_center_3D=centroids3D[index]
print("Chosen coord center:")
print(chosen_coord_center_3D)

# visualize the mask
"""
plt.figure("detection_mask")
plt.imshow(detection_mask)
plt.show()
"""

# transform the 3D cetroids to roughly match the orientation of skin model
T=transformation_matrix(0,0,0,-np.pi/2,0,np.pi/2)
centroids3D-=chosen_coord_center_3D
centroids3D=np.dot(centroids3D,T[0:3,0:3].T)

# 3D scatter plot the centroids
fig=plt.figure("3D centroids")
ax=fig.add_subplot(projection="3d")
ax.scatter(centroids3D[:,0],centroids3D[:,1],centroids3D[:,2])
plt.show()

# save numpy point clouds
np.save("lower_right_leg1_2D.npy",centroids)
np.save("lower_right_leg1_3D.npy",centroids3D)
