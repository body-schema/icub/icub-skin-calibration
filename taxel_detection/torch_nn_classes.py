from torch import nn

class mlp(nn.Module):
    """Basic MLP example from pytorch tutorial"""
    def __init__(self):
        super(mlp, self).__init__()
        self.flatten = nn.Flatten()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(3*16*16, 512),
            nn.ReLU(),
            nn.Linear(512, 512),
            nn.ReLU(),
            nn.Linear(512, 1),
        )

    def forward(self, x):
        x = self.flatten(x)
        logits = self.linear_relu_stack(x)
        return logits

class cnn1(nn.Module):
    def __init__(self):
        super(cnn1, self).__init__()
        self.conv1=nn.Conv2d(3,6,5)
        self.lin1=nn.Linear(6*12*12,256)
        self.lin2=nn.Linear(256,128)
        self.lin3=nn.Linear(128,1)
        self.relu=nn.ReLU()
        self.flatten=nn.Flatten()
    
    def forward(self,x):
        x=self.relu(self.conv1(x))
        x=self.flatten(x)
        x=self.relu(self.lin1(x))
        x=self.relu(self.lin2(x))
        logits=self.lin3(x)
        return logits

class cnn2(nn.Module):
    """inspired by LeNet5"""
    def __init__(self):
        super(cnn2, self).__init__()
        self.conv1=nn.Conv2d(3,6,5)
        self.conv2=nn.Conv2d(6,16,3)
        self.lin1=nn.Linear(16*10*10,120)
        self.lin2=nn.Linear(120,60)
        self.lin3=nn.Linear(60,1)
        self.relu=nn.ReLU()
        self.flatten=nn.Flatten()
    
    def forward(self,x):
        x=self.relu(self.conv1(x))
        x=self.relu(self.conv2(x))
        x=self.flatten(x)
        x=self.relu(self.lin1(x))
        x=self.relu(self.lin2(x))
        x=self.lin3(x)
        return x

class cnn3(nn.Module):
    def __init__(self):
        super(cnn3, self).__init__()
        self.conv1=nn.Conv2d(3,6,3)
        self.conv2=nn.Conv2d(6,16,5)
        self.lin1=nn.Linear(16*10*10,128)
        self.lin2=nn.Linear(128,64)
        self.lin3=nn.Linear(64,1)
        self.relu=nn.ReLU()
        self.flatten=nn.Flatten()
    def forward(self,x):
        x=self.relu(self.conv1(x))
        x=self.relu(self.conv2(x))
        x=self.flatten(x)
        x=self.relu(self.lin1(x))
        x=self.relu(self.lin2(x))
        x=self.lin3(x)
        return x

class cnn4(nn.Module):
    def __init__(self):
        super(cnn4, self).__init__()
        self.conv1=nn.Conv2d(3,6,3)
        self.conv2=nn.Conv2d(6,16,5)
        self.conv3=nn.Conv2d(16,24,5)
        self.lin1=nn.Linear(24*6*6,128)
        self.lin2=nn.Linear(128,64)
        self.lin3=nn.Linear(64,1)
        self.relu=nn.ReLU()
        self.flatten=nn.Flatten()
    def forward(self,x):
        x=self.relu(self.conv1(x))
        x=self.relu(self.conv2(x))
        x=self.relu(self.conv3(x))
        x=self.flatten(x)
        x=self.relu(self.lin1(x))
        x=self.relu(self.lin2(x))
        x=self.lin3(x)
        return x

class cnn5(nn.Module):
    def __init__(self):
        super(cnn5, self).__init__()
        self.conv1=nn.Conv2d(3,6,3)
        self.conv2=nn.Conv2d(6,16,3)
        self.conv3=nn.Conv2d(16,24,5)
        self.conv4=nn.Conv2d(24,32,5)
        self.lin1=nn.Linear(32*4*4,128)
        self.lin2=nn.Linear(128,64)
        self.lin3=nn.Linear(64,1)
        self.relu=nn.ReLU()
        self.flatten=nn.Flatten()
    def forward(self,x):
        x=self.relu(self.conv1(x))
        x=self.relu(self.conv2(x))
        x=self.relu(self.conv3(x))
        x=self.relu(self.conv4(x))
        x=self.flatten(x)
        x=self.relu(self.lin1(x))
        x=self.relu(self.lin2(x))
        x=self.lin3(x)
        return x
