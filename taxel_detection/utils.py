import numpy as np
import matplotlib.pyplot as plt

"""solution for unequal axes from https://stackoverflow.com/questions/13685386/how-to-set-the-equal-aspect-ratio-for-all-axes-x-y-z"""
def set_axes_equal(ax):
    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = abs(x_limits[1] - x_limits[0])
    x_middle = np.mean(x_limits)
    y_range = abs(y_limits[1] - y_limits[0])
    y_middle = np.mean(y_limits)
    z_range = abs(z_limits[1] - z_limits[0])
    z_middle = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.5*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])

"""rotation matrix from roll-pitch-yaw"""
def rot_matrix(r,p,y):
    Rz=np.array([[np.cos(y), -np.sin(y), 0],
                 [np.sin(y), np.cos(y), 0],
                 [0, 0, 1]])
    Ry=np.array([[np.cos(p), 0, np.sin(p)],
                 [0, 1, 0],
                 [-np.sin(p), 0, np.cos(p)]])
    Rx=np.array([[1, 0, 0],
                 [0, np.cos(r), -np.sin(r)],
                 [0, np.sin(r), np.cos(r)]])
    R=np.linalg.multi_dot((Rz,Ry,Rx))
    return R

"""plot coordinate system"""
def plot_system(ax, system):
    ax.plot([system[0,0],system[0,3]],[system[1,0],system[1,3]],[system[2,0],system[2,3]],c='blue')
    ax.plot([system[0,0],system[0,1]],[system[1,0],system[1,1]],[system[2,0],system[2,1]],c='red')
    ax.plot([system[0,0],system[0,2]],[system[1,0],system[1,2]],[system[2,0],system[2,2]],c='green')

"""build a transformation matrix from x,y,z,roll,pitch,yaw"""
def transformation_matrix(x,y,z,r,p,yaw):
    T=np.eye(4)
    R=rot_matrix(r,p,yaw)
    t=np.array([[x],[y],[z]])
    T[0:3,0:3]=R
    T[0:3,3:4]=t
    return T

"""homogenize vectors for transforming"""
def homogenize(x):
    x_h=np.vstack((x,np.ones((1,x.shape[1]))))
    return x_h
