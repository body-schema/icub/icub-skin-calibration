"""TAXEL DATASET COLLECTOR"""
"""
A tool to extract taxel cutouts for NN dataset: run the code and click on
various places in the picture to save a cutout
Current setting: every 4th label is 1 * (every fourth cutout is a taxel)
Cut out a taxel by clicking in its center.
"""
#BUG: after the last picture exits without saving the csv file with labels
# use the commented code below to add missing lines until this is fixed

import cv2
import csv

cutout_size=16 #cutout square size in pixels
cutouts_per_pic=40 #cutouts per picture
pics_in_folder=20 #number of pictures to make cutouts from

def click_event(event,x,y,flags,params):
    global num_pictures, num_cutouts, img, i, num_cutouts, label
    if event == cv2.EVENT_LBUTTONDOWN:
        cutout=img[y-cutout_size//2:y+cutout_size//2,x-cutout_size//2:x+cutout_size//2]
        filename=(str(cutouts_per_pic*i+num_cutouts)+".jpg")
        cv2.imwrite('dataset/taxel_dataset5/{}.jpg'.format(cutouts_per_pic*i+num_cutouts),cutout) #save cutout
        csvwriter.writerow([filename,label]) #save label
        num_cutouts+=1
        if num_cutouts%4==3: label=1
        else: label=0
        if num_cutouts>cutouts_per_pic-1:
            i+=1
            num_cutouts=0
            if(i>pics_in_folder-1): exit()
            img=cv2.imread("dataset/shots1_3_1280x720/"+str(i)+".jpg")
            cv2.imshow("image",img)

i=19
num_cutouts=0
label=0

with open("dataset/taxel_labels5.csv","a",newline='') as csvfile:
    csvwriter=csv.writer(csvfile)
    #csvwriter.writerow(["filename","label"])
    img=cv2.imread("dataset/shots1_3_1280x720/"+str(i)+".jpg")
    cv2.imshow("image",img)
    cv2.setMouseCallback('image',click_event)
    cv2.waitKey(0)

cv2.destroyAllWindows()
"""
with open("dataset/taxel_labels4.csv","a",newline='') as csvfile:
    csvwriter=csv.writer(csvfile)
    for i in range(680,800):
        filename=(str(i)+".jpg")
        if i%4==3: label=1
        else: label=0
        csvwriter.writerow([filename,label])
"""
