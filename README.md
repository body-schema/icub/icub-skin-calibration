# iCub skin calibration

# Calibration from CAD

```skin_models``` folder contains the source code and files for making taxel position files from:
- CAD models of the skin holders, or
- iCub's kinematic model (```model.urdf```)

The triangle positions and orientations are determined based on circular marks on the holder, which should correspond with the triangles' center (taxel #3). The curvature of individual triangles is neglected.

Steps:
1) Make coordinate systems in CAD for each triangle
2) Generate taxel point cloud based on the transformations
3) Create taxel position text file

Used files from robotology github repo:
- iCub's kinematic model: [icub-models/iCub/robots/iCubGazeboV2_7/model.urdf](https://github.com/robotology/icub-models/blob/master/iCub/robots/iCubGazeboV2_7/model.urdf)
- skinGui configuration files from [icub-main/app/skinGui/conf/skinGui](https://github.com/robotology/icub-main/tree/master/app/skinGui/conf/skinGui)


# Calibration by 3D reconstruction

```taxel_detection``` folder contains the framework for taxel detection in an RGBD camera input. For more details refer to the [diploma thesis](https://dspace.cvut.cz/handle/10467/108574).