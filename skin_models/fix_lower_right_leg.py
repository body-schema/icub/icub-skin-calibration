"""
Fix incorrectly placed triangles 19,20,21 in lower_right_leg.ini
"""
import numpy as np
from triangle import Triangle
from read_ini import triangles3D_from_ini, triangles2D_from_ini
import matplotlib.pyplot as plt

def get_fixed_lower_right_leg3D():
    inifile="right_leg_lower.ini"
    triangles3D=triangles3D_from_ini(inifile)

    # remove the extra triangle 19
    del triangles3D[-1]

    tr_ids=np.array([t.ID for t in triangles3D])

    # fix triangle 19
    idx=np.where(tr_ids==19)[0][0]
    triangles3D[idx]=Triangle(19,np.array([80, 19, 0]),180)

    # fix triangle 20
    idx=np.where(tr_ids==20)[0][0]
    triangles3D[idx]=Triangle(20,np.array([96, 10, 0]),240)

    # fix triangle 21
    idx=np.where(tr_ids==21)[0][0]
    triangles3D[idx]=Triangle(21,np.array([96, -9,  0]),180)

    return triangles3D

def get_fixed_lower_right_leg():
    inifile="right_leg_lower.ini"
    triangles2D=triangles2D_from_ini(inifile)

    # remove the extra triangle 19
    triangles2D=np.delete(triangles2D,-1,axis=0)

    tr_ids=triangles2D[:,0]

    # fix triangle 19
    idx=np.where(tr_ids==19)[0][0]
    triangles2D[idx]=[19,80,19,180]
    
    # fix triangle 20
    idx=np.where(tr_ids==20)[0][0]
    triangles2D[idx]=[20,96,10,240]

    # fix triangle 21
    idx=np.where(tr_ids==21)[0][0]
    triangles2D[idx]=[21,96,-9,180]
    
    return triangles2D

if __name__=="__main__":
    
    # 3D
    triangles3D=get_fixed_lower_right_leg3D()
    fig=plt.figure("3D triangles")
    ax=fig.add_subplot(projection="3d")
    for t in triangles3D:
        taxels=t.get_taxels()
        ax.scatter(taxels[:,0],taxels[:,1],taxels[:,2])
    # label triangles
    tr_ids=np.array([t.ID for t in triangles3D])
    tr_poses=np.array([t.pos for t in triangles3D])
    for i,txt in enumerate(tr_ids):
        label=int(txt)
        ax.text(tr_poses[i,0], tr_poses[i,1], tr_poses[i,2], label)
    
    """
    # 2D
    triangles=get_fixed_lower_right_leg()
    plt.figure()
    plt.scatter(triangles[:,1],triangles[:,2])
    for i, txt in enumerate(triangles[:,0]):
        plt.annotate(txt, (triangles[i,1], triangles[i,2]))
    """
plt.show()
