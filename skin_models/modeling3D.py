"""
Gathers triangle centers and represents them as Triangle objects (see Triangle
class in triangle.py). Then plots all their taxels and labels triangle IDs or
taxel IDs. The orientation of triangles are extracted from torso.ini.
Function addTriangle3DCenter and torso triangle poses copied from
torsoTrianglesCAD.py in robotology-playground repo and edited for our purposes.

Use rotateTriangle function to rotate triangles around their centers.
"""

import numpy as np
import matplotlib.pyplot as plt
from triangle import Triangle
from utils import set_axes_equal
from read_ini import triangles2D_from_ini

def addTriangle3DCenter(triangleNumber, x, y, z, triangle_list, ini_list):
    index_in_ini_list=np.where(ini_list[:,0]==triangleNumber)[0][0]
    triangle3D=Triangle(triangleNumber, np.array([x,y,z]), ini_list[index_in_ini_list,3])
    triangle_list.append(triangle3D)

def rotateTriangle(triangle_list,triangleID,rpy):
    IDs_in_list=np.array([t.ID for t in triangle_list])
    if(triangleID in IDs_in_list):
        index=np.where(IDs_in_list==triangleID)[0][0]
        triangle_list[index].rotate_rpy(rpy[0],rpy[1],rpy[2])

triangles3D=[]
triangles=triangles2D_from_ini("torso.ini")

# triangle centers of torso skin - taken from torsoTrianglesCAD.py
addTriangle3DCenter(5,-49,0,103,triangles3D, triangles)
addTriangle3DCenter(6,-49,34,86,triangles3D, triangles)
addTriangle3DCenter(7,-46,44,70,triangles3D, triangles)
addTriangle3DCenter(8,-16,36,90,triangles3D, triangles)
addTriangle3DCenter(9,-33,29,96,triangles3D, triangles)
addTriangle3DCenter(10,-33,12,105,triangles3D, triangles)
addTriangle3DCenter(11,-16,3,109,triangles3D, triangles)
addTriangle3DCenter(17,-62,-60,92,triangles3D, triangles)
addTriangle3DCenter(18,-62,-80,89,triangles3D, triangles)
addTriangle3DCenter(19,-48,-89,97,triangles3D, triangles)
addTriangle3DCenter(22,0,-23,113,triangles3D, triangles)
addTriangle3DCenter(23,17,-33,113,triangles3D, triangles)
addTriangle3DCenter(24,-17,-52,112,triangles3D, triangles)
addTriangle3DCenter(25,-17,-33,112,triangles3D, triangles)
addTriangle3DCenter(26,-33,-23,110,triangles3D, triangles)
addTriangle3DCenter(27,-49,-33,105,triangles3D, triangles)
addTriangle3DCenter(28,-49,-52,104,triangles3D, triangles)
addTriangle3DCenter(29,-33,-62,108,triangles3D, triangles)
addTriangle3DCenter(32,62,-60,93,triangles3D, triangles)
addTriangle3DCenter(33,49,-52,105,triangles3D, triangles)
addTriangle3DCenter(34,49,-33,106,triangles3D, triangles)
addTriangle3DCenter(35,-16,-110,104,triangles3D, triangles)
addTriangle3DCenter(36,0,-120,105,triangles3D, triangles)
addTriangle3DCenter(37,16,-109,104,triangles3D, triangles)
addTriangle3DCenter(38,-17,-91,108,triangles3D, triangles)
addTriangle3DCenter(39,-33,-81,106,triangles3D, triangles)
addTriangle3DCenter(40,0,-61,112,triangles3D, triangles)
addTriangle3DCenter(41,0,-81,110,triangles3D, triangles)
addTriangle3DCenter(42,17,-91,108,triangles3D, triangles)
addTriangle3DCenter(43,33,-81,106,triangles3D, triangles)
addTriangle3DCenter(44,33,-62,109,triangles3D, triangles)
addTriangle3DCenter(45,17,-52,112,triangles3D, triangles)
addTriangle3DCenter(46,48,-89,97,triangles3D, triangles)
addTriangle3DCenter(47,62,-80,89,triangles3D, triangles)
addTriangle3DCenter(49,49,34,86,triangles3D, triangles)
addTriangle3DCenter(50,46,44,70,triangles3D, triangles)
addTriangle3DCenter(52,33,-23,111,triangles3D, triangles)
addTriangle3DCenter(56,0,30,98,triangles3D, triangles)
addTriangle3DCenter(57,0,13,107,triangles3D, triangles)
addTriangle3DCenter(58,16,3,109,triangles3D, triangles)
addTriangle3DCenter(59,33,12,105,triangles3D, triangles)
addTriangle3DCenter(60,33,29,96,triangles3D, triangles)
addTriangle3DCenter(61,16,36,90,triangles3D, triangles)
addTriangle3DCenter(62,48,0,102,triangles3D, triangles)

tr_ids=np.array([t.ID for t in triangles3D])
tr_poses=np.array([t.pos for t in triangles3D])

# rotation test
#rotateTriangle(triangles3D,8,[np.pi/2.,0.,0.])

fig=plt.figure("3D centers")
ax=fig.add_subplot(projection="3d")
for t in triangles3D:
    taxels=t.get_taxels()
    ax.scatter(taxels[:,0],taxels[:,1],taxels[:,2])
    # label triangles
    for i,txt in enumerate(tr_ids):
        label=int(txt)
        ax.text(tr_poses[i,0], tr_poses[i,1], tr_poses[i,2], label)
    """
    # label taxels
    for i, txt in enumerate(taxels[:,3]):
        label=int(txt)
        ax.text(taxels[i,0], taxels[i,1], taxels[i,2], label)
    """
set_axes_equal(ax)
plt.show()
