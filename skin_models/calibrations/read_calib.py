"""reads calibration from text file and plots the taxels"""
import re
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('..')
from utils import set_axes_equal

#create numpy array for positions
taxels=np.zeros((1,3))

f=open("right_leg_upper.txt","r") #right_leg_lower, left_leg_lower, left_leg_upper, right_leg_upper
line=f.readline()

#skip the header
while(not "[calibration]" in line):
    line=f.readline()
line=f.readline()

#read the values
while(line!=""):
    #coords=line.split(" ") #" ", "\t"
    coords=re.findall(r'[-+]?\d+\.\d+',line)
    if not (float(coords[0])==0. and float(coords[1])==0. and float(coords[2])==0.):
        taxels=np.append(taxels,[[float(coords[0]),float(coords[1]),float(coords[2])]],axis=0)
    line=f.readline()

f.close()

#plot the taxels
plt.figure()
ax=plt.axes(projection="3d")
ax.scatter3D(taxels[:,0],taxels[:,1],taxels[:,2])
set_axes_equal(ax)
plt.show()
