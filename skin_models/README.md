# Calibration from CAD and kinematic model

```CAD``` folder contains skin holders' CAD models with individual triangles' coordinate systems, which were added manually.

```calibrations``` folder contains the position files and ```read_calib.py``` script for visualization

Calibrations created from CAD models:
- ```left_leg_lower.txt```
- ```left_leg_upper.txt```
- ```right_leg_lower.txt```
- ```right_leg_upper.txt```
- ```left_arm.txt```
- ```right_arm.txt```

Calibrations created from kinematic (.urdf) model:
- ```left_foot.txt```
- ```right_foot.txt```

```CStransformations_*.npy``` and ```CSlabels_*.npy``` files contain transformations to triangle centers extracted from CAD files and corresponding triangle IDs, respectively.

## Code
- ```fix_lower_right_leg.py``` - Extracts triangles from lower right leg ini file and corrects triangles 19,20,21 (this code is redundant now, the ini file has been fixed).
- ```leg_skin_from_urdf_and_3Drec.py``` - Extracts triangles of right lower leg skin from urdf and makes the left lower leg by mirroring them. This code was used before the CAD models were provided. Works also with point clouds obtained by 3D reconstruction.
- ```modelling3D.py``` - Creates and plots a 3D model of torso skin using coordinates provided in torsoTrianglesCAD.py (from robotology Github).
- read_ini.py - Contains functions to extract 2D triangle layout from ini files and return the triangles as a list.
- ```recover_transformation.py``` - Function to recover affine homogenous transformation from two sets of 3 points. It is used in leg_skin_from_urdf_and_3Drec.py to create transformations for lower left leg.
- ```skin_CS.py``` - Loads and plots coordinate systems of triangles extracted from CAD.
- ```skin_from_urdf.py``` - Creates taxel position text files using infromation from ini files and triangle transformations from model.urdf. Plotting included.
- ```skin_from_CS.py``` - Creates taxel position text files using information from ini files and triangle transformations extracted from CAD. Plotting included.
- ```triangle.py``` - Contains Triangle class and its methods plus a function to create calibration text file from a list of triangles and a function to plot a list of triangles.
- ```utils.py``` - Contains helping functions for plotting and transformations.
