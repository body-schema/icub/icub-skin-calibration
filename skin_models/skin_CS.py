"""
Loads and plots coordinate systems of triangles extracted from CAD
"""
import numpy as np
from utils import homogenize, transformation_matrix, plot_system, set_axes_equal
import matplotlib.pyplot as plt

systems=np.zeros((1,3,4))
systems[0]=np.array([[0,10,0,0],
            [0,0,10,0],
            [0,0,0,10]])

# transformations
T_r_knee=transformation_matrix(-70.1,-529.9384739142,11.38241964656,0,-np.pi/2,0) # from CAD origin to right knee, retrieved from r_lower_leg_cover.stp: lines 164646-164649
T_l_knee=transformation_matrix(70.1,-529.9384739142,11.38241964655,0,-np.pi/2,0) # from CAD origin to left knee, retrieved from l_lower_leg_cover.stp: lines 157639-157642
T_r_knee2r_lower_leg=transformation_matrix(1000*0.0012363999999999917, -1000*0.003513100000000033, -1000*0.08879400000000004, 1.5707963267948961, 0, 1.5707963267948961) # from right knee to r_lower_leg
Tr_knee2urdf=transformation_matrix(0,0,0,-np.pi/2,0,0) # from right knee to CS from urdf

# load transformations and labels
transformations=np.load("CStransformations_lll.npy") #rll/lll
labels=np.load("CSlabels_lll.npy") #rll/lll

# create coordinate systems
for i in range(1,transformations.shape[0]):
    T=transformations[i,:,:] #in base frame from CAD
    #new_system=np.matmul(T,homogenize(systems[0]))[0:3,:] #triangle CS in CAD origin CS
    #new_system=np.matmul(np.dot(np.linalg.inv(T_r_knee),T),homogenize(systems[0]))[0:3,:] #triangle CS in right knee CS
    #new_system=np.matmul(np.dot(np.linalg.inv(T_l_knee),T),homogenize(systems[0]))[0:3,:] #triangle CS in left knee CS
    #new_system=np.matmul(np.dot(np.linalg.inv(np.dot(T_r_knee,T_r_knee2r_lower_leg)),T),homogenize(systems[0]))[0:3,:] #triangle CS in lower leg CS
    new_system=np.matmul(np.dot(np.linalg.inv(np.dot(T_l_knee,Tr_knee2urdf)),T),homogenize(systems[0]))[0:3,:] #triangle CS in CS from urdf
    systems=np.append(systems,[new_system],axis=0)

# plot coordinate systems
fig=plt.figure("coord. systems")
ax=fig.add_subplot(projection="3d")
for system in systems:#[1:]
    plot_system(ax, system)
for i, txt in enumerate(labels[1:]):
    label=int(txt)
    ax.text(systems[i+1,0,0],systems[i+1,1,0],systems[i+1,2,0],label)
#lower_leg_system=np.dot(T_r_knee2r_lower_leg,homogenize(systems[0]))[0:3,:]
#plot_system(ax,lower_leg_system)
set_axes_equal(ax)
#ax.view_init(0,90)
plt.show()

#np.save("systems.npy",systems)
