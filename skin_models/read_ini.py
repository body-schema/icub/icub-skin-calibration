import re
import numpy as np
import matplotlib.pyplot as plt
from triangle import Triangle, plot3DTriangles

def triangles3D_from_ini(inifile):
    begin_from="triangle_10pad"

    triangles3D=[]

    skinpartfile=open(inifile,"r")
    line=" "
    while line:
        line=skinpartfile.readline()
        if(begin_from in line):
            res = re.findall(r'-?\d+', line[len(begin_from):])
            triangle3D=Triangle(int(res[0]), np.array([int(res[1]),int(res[2]),0]), int(res[3])) #3D
            triangles3D.append(triangle3D)
    skinpartfile.close()
    return triangles3D

def triangles2D_from_ini(inifile):
    begin_from="triangle_10pad"

    triangles=np.zeros([0,4],dtype=int) # ID, X, Y, orientation(deg)

    skinpartfile=open(inifile,"r")
    line=" "
    while line:
        line=skinpartfile.readline()
        if(begin_from in line):
            res = re.findall(r'-?\d+', line[len(begin_from):])
            triangles=np.append(triangles,[[int(res[0]),int(res[1]),int(res[2]),int(res[3])]],axis=0) #2D

    skinpartfile.close()
    return triangles

if __name__ == '__main__':
    inifile="right_arm.ini" #torso, left_leg_lower, left_leg_upper, right_leg_lower, right_leg_upper, right_foot, left_foot
    triangles3D=triangles3D_from_ini(inifile)
    plot3DTriangles(triangles3D,"3D triangles right",1,True)
    
    #triangles=triangles2D_from_ini(inifile)
    """
    # plot 2D centers
    plt.figure()
    plt.scatter(triangles[:,1],triangles[:,2])
    for i, txt in enumerate(triangles[:,0]):
        plt.annotate(txt, (triangles[i,1], triangles[i,2]))
    plt.show()
    """