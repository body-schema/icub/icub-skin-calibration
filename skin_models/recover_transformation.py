import numpy as np

def transform_pt(point, trans_mat):
    a  = np.array([point[0], point[1], point[2], 1])
    ap = np.dot(a, trans_mat)[:3]
    return np.array([ap[0], ap[1], ap[2]]).T

def recover_homogenous_affine_transformation(p, p_prime):
    '''
    Find the unique homogeneous affine transformation that
    maps a set of 3 points to another set of 3 points in 3D
    space:

        p_prime == np.dot(p, R) + t

    where `R` is an unknown rotation matrix, `t` is an unknown
    translation vector, and `p` and `p_prime` are the original
    and transformed set of points stored as row vectors:

        p       = np.array((p1,       p2,       p3))
        p_prime = np.array((p1_prime, p2_prime, p3_prime))

    The result of this function is an augmented 4-by-4
    matrix `A` that represents this affine transformation:

        np.column_stack((p_prime, (1, 1, 1))) == \
            np.dot(np.column_stack((p, (1, 1, 1))), A)

    Source: https://math.stackexchange.com/a/222170 (robjohn)
    '''

    # construct intermediate matrix
    Q       = p[1:]       - p[0]
    Q_prime = p_prime[1:] - p_prime[0]

    # calculate rotation matrix
    R = np.dot(np.linalg.inv(np.row_stack((Q, np.cross(*Q)))),
               np.row_stack((Q_prime, np.cross(*Q_prime))))

    # calculate translation vector
    t = p_prime[0] - np.dot(p[0], R)

    # calculate affine transformation matrix
    return np.column_stack((np.row_stack((R, t)),
                            (0, 0, 0, 1)))

if __name__ == '__main__':
    p=np.array([[ 1.,  1.,  1.],
       [ 1.,  2.,  1.],
       [ 1.,  1.,  2.]])
    
    trans_mat = np.array([[0.866025403784, -0.353553390593, -0.353553390593, 0],
                          [0.353553390593,  0.933012701892, -0.066987298108, 0],
                          [0.353553390593, -0.066987298108,  0.933012701892, 0],
                          [0.841081377402,  5.219578794378,  0.219578794378, 1]])
    """
    p_prime=np.array([[ 2.41421356,  5.73205081,  0.73205081],
           [ 2.76776695,  6.66506351,  0.66506351],
           [ 2.76776695,  5.66506351,  1.66506351]])
    """
    p_prime=transform_pt(p,trans_mat)
    
    T=recover_homogenous_affine_transformation(p,p_prime)
    print(T)
