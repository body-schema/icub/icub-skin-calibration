import numpy as np
import matplotlib.pyplot as plt
from utils import set_axes_equal, rot_matrix

class Triangle():
    def __init__(self, ID, pos=np.zeros((3)), orientation=0):
        self.ID=ID
        self.pos=pos
        self.orientation=orientation
        rot_matrix=np.array([[np.cos(np.deg2rad(orientation)),-np.sin(np.deg2rad(orientation))],
                             [np.sin(np.deg2rad(orientation)),np.cos(np.deg2rad(orientation))]])
        self.taxels=np.array([
            [6.533,0.],
            [9.8,-5.66],
            [3.267,-5.66],
            [0.,0.],
            [-3.267,-5.66],
            [-9.8,-5.66],
            [-6.533,0.],
            [-3.267,5.66],
            [0.,11.317],
            [3.267,5.66]])
        if not orientation==0:
            self.taxels[:,0:2]=np.dot(self.taxels[:,0:2],rot_matrix.T)
        self.taxels=np.append(self.taxels,np.zeros((10,1)),axis=1) #add Z coordinates (zeros)
        self.taxels=self.taxels+pos #add origin position
        self.taxels=np.append(self.taxels,[[0],[1],[2],[3],[4],[5],[7],[8],[9],[11]],axis=1) # IDs
    def get_taxels(self):
        return self.taxels
    def get_orientation(self):
        return self.orientation
    def get_pos(self):
        return self.pos
    def rotate_rpy(self,r,p,y):
        """rotate around the center taxel"""
        R=rot_matrix(r,p,y)
        self.taxels[:,0:3]=np.dot(self.taxels[:,0:3]-self.pos,R.T)+self.pos
    def transform_T(self,tr,rpy):
        """transform by given translation and rotation"""
        R=rot_matrix(rpy[0],rpy[1],rpy[2])
        t=np.array([[tr[0]],[tr[1]],[tr[2]]])
        
        self.taxels[:,0:3]=np.dot(self.taxels[:,0:3]-self.pos,R.T)+self.pos+t.T
        self.pos+=[tr[0],tr[1],tr[2]]

def triangleList2txtCalib(triangles3D,filename,name,num_patches):
    # calibration text file creation
    calibration=np.zeros((num_patches*16*12,6))
    tr_ids=np.array([t.ID for t in triangles3D])
    for triangle in triangles3D:
        taxels=triangle.get_taxels()
        for taxel in taxels:
            taxel_index=int(triangle.ID*12+taxel[3])
            calibration[taxel_index,:]=np.hstack((0.001*taxel[0:3],[1,0,0]))
        # thermal taxels
        calibration[triangle.ID*12+6,:]=np.array([0,0,0,0,0,0])
        calibration[triangle.ID*12+10,:]=np.array([0,0,0,0,0,0])

    file=open(filename,"w")
    file.write("name\t"+name+"\n")
    #file.write("frame\t"+frame+"\n")
    file.write("spatial_sampling\ttaxel\n")
    file.write("taxel2Repr ( ")
    for i in range(5*16):
        if i in tr_ids:
            center=i*12+3
            file.write(" %d  %d  %d  %d  %d  %d " % (center, center, center, center, center, center))
            file.write(" -2 ")
            file.write(" %d  %d  %d " % (center, center, center))
            file.write(" -2 ")
            file.write(" %d " % center)
        else:
            for j in range(12):
                file.write(" -1 ")
    file.write(")\n")
    file.write("[calibration]\n")
    for row in calibration:
        line="%f %f %f %f %f %f\n" % (row[0],row[1],row[2],row[3],row[4],row[5])
        file.write(line)
    file.close()

def plot3DTriangles(triangles3D,title="3D triangles",labeling=0,show=True):
    """plot triangles from a triangle list
    arguments:
    triangles3D - list of triangles
    title - figure title
    labeling: 0 = no labels, 1 = label triangles, 2 = label taxels
    show - call plt.show() at the end? (bool)
    """
    fig=plt.figure(title)
    ax=fig.add_subplot(projection="3d")
    for t in triangles3D:
        taxels=t.get_taxels()
        ax.scatter(taxels[:,0],taxels[:,1],taxels[:,2])

        #label taxels if requested
        if labeling==2:
            for i, txt in enumerate(taxels[:,3]):
                label=int(txt)
                ax.text(taxels[i,0], taxels[i,1], taxels[i,2], label)
    #label triangles if requested
    if labeling==1:
        tr_ids=np.array([t.ID for t in triangles3D])
        tr_poses=np.array([t.pos for t in triangles3D])
        for i,txt in enumerate(tr_ids):
            label=int(txt)
            ax.text(tr_poses[i,0], tr_poses[i,1], tr_poses[i,2], label)

    set_axes_equal(ax)
    if show: plt.show()
    return ax

if __name__ == '__main__':
    t1=Triangle(0,np.array([5.,5.,5.]),90)
    ax=plot3DTriangles([t1],"3D triangles",2,False)

    """
    # rotate_rpy test
    t1.rotate_rpy(0,np.pi/4,0)
    #t1.transform_T(np.array([1,1,1]),np.array([np.pi/4,np.pi/4,np.pi/4]))

    plot3DTriangles([t1],"3D triangles 2",2)
    """
