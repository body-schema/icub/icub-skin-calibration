# extract position dictionary from urdf
with open("model.urdf", "r") as f:
    data = f.read()
i = 0
triangles = {}
while True:
    link = data.find("skin", i)
    if "child" in data[link-50:link]:
        i = link+1
        continue
    if link == -1:
        break
    name_start = data.find("link name=", link-50)
    name_end = data.find('"/>', name_start)
    name = data[name_start+11:name_end]
    origin_start = data.find("origin xyz", link)
    origin_end = data.find('"/>', origin_start)
    coords = data[origin_start+12:origin_end]
    coords = list(map(float, coords.replace('" rpy="', ' ').split(" ")))
    triangles[name] = coords
    i = origin_end

# create dictionary of mirroring triangles from right leg to left leg
lrl2lll={}
lrl2lll[19]=16
lrl2lll[20]=17
lrl2lll[21]=28
lrl2lll[6]=14
lrl2lll[3]=0
lrl2lll[4]=15
lrl2lll[5]=5
lrl2lll[15]=4
lrl2lll[0]=3
lrl2lll[31]=23
lrl2lll[16]=22
lrl2lll[17]=21
lrl2lll[28]=20
lrl2lll[14]=6
lrl2lll[11]=9
lrl2lll[10]=10
lrl2lll[9]=11
lrl2lll[46]=38
lrl2lll[43]=41
lrl2lll[42]=42
lrl2lll[41]=43
lrl2lll[38]=46
lrl2lll[29]=19
lrl2lll[35]=32
lrl2lll[36]=47
lrl2lll[37]=37
lrl2lll[47]=36
lrl2lll[32]=35
lrl2lll[50]=54
lrl2lll[51]=53
lrl2lll[52]=52
lrl2lll[53]=51
lrl2lll[54]=50
lrl2lll[55]=49
lrl2lll[56]=60
lrl2lll[61]=61
lrl2lll[60]=56
lrl2lll[49]=55

import numpy as np
from triangle import Triangle, triangleList2txtCalib
from read_ini import triangles2D_from_ini
import matplotlib.pyplot as plt
from utils import rot_matrix, set_axes_equal, plot_system, transformation_matrix, homogenize
#import lrl_to_lll_dict
from recover_transformation import recover_homogenous_affine_transformation
from fix_lower_right_leg import get_fixed_lower_right_leg

# ID of chosen center point
centerID=10
coords=triangles["r_lower_leg_skin_"+str(centerID)]
center_coords=np.array([coords[0],coords[1],coords[2]])*1000
#T_to_center_coords=transformation_matrix(coords[0],coords[1],coords[2],0,0,0)

#ids and orientations taken by read_ini.py
#triangles2D=triangles2D_from_ini("right_leg_lower.ini")
triangles2D=get_fixed_lower_right_leg()
lower_right_leg_triangles=triangles2D[:,0]
lower_right_leg_orientations=triangles2D[:,3]

triangles2D_left=triangles2D_from_ini("left_leg_lower.ini")
lower_left_leg_triangles=triangles2D_left[:,0]
lower_left_leg_orientations=triangles2D_left[:,3]

# initialize coordinate systems
systems=np.zeros((lower_right_leg_triangles.shape[0]+1,3,4))
systems[0]=np.array([[0,10,0,0],
            [0,0,10,0],
            [0,0,0,10]])

systems2=np.zeros((lower_right_leg_triangles.shape[0]+1,3,4))

triangles3D=[]
triangles_ll_3D=[] # left leg
i=0
for ID in lower_right_leg_triangles:
    if ID==43: # note: triangles 43 and 46 are swapped (wrong) in urdf!
        coords=triangles["r_lower_leg_skin_46"]
    elif ID==46:
        coords=triangles["r_lower_leg_skin_43"]
    else:
        coords=triangles["r_lower_leg_skin_"+str(ID)]
    
    triangle3D=Triangle(ID,np.zeros((3,)),lower_right_leg_orientations[i])
    triangle3D.rotate_rpy(0.,0.,-np.pi/2.)
    triangle3D.transform_T([coords[0]*1000,coords[1]*1000,coords[2]*1000],[coords[3],coords[4],coords[5]])
    #triangle3D.transform_T([-center_coords[0],-center_coords[1],-center_coords[2]],[0,0,0])
    
    #triangle3D=Triangle(ID,np.array([coords[0]*1000,coords[1]*1000,coords[2]*1000]),lower_leg_orientations[i])
    #triangle3D.rotate_rpy(np.pi/2.,0.,np.pi/2.)
    #triangle3D.rotate_rpy(coords[3],coords[4],coords[5])
    triangles3D.append(triangle3D)
    
    # calculate coordinate systems
    T=transformation_matrix(coords[0]*1000,coords[1]*1000,coords[2]*1000,coords[3],coords[4],coords[5])
    new_system=np.matmul(T,homogenize(systems[0]))[0:3,:]
    systems[i+1]=new_system
    
    # left leg !
    system2=np.copy(new_system)
    system2[0,:]=-system2[0,:]
    system2[:,2]=system2[:,2]-2*(system2[:,2]-system2[:,0])

    rot_z=rot_matrix(0,0,np.pi)
    system2=np.dot(rot_z,system2)
    
    p_prime=np.array([system2[:,1],system2[:,2],system2[:,3]])
    T2=recover_homogenous_affine_transformation(systems[0,:,1:4],p_prime).T
    new_system2=np.matmul(T2,homogenize(systems[0]))[0:3,:]
    systems2[i]=new_system2

    index=np.where(lower_left_leg_triangles==lrl2lll[ID])[0][0]
    triangle_ll_3D=Triangle(lrl2lll[ID],np.zeros((3,)),lower_left_leg_orientations[index])
    triangle_ll_3D.rotate_rpy(0.,0.,-np.pi/2.)
    triangle_ll_3D.taxels[:,0:3]=np.dot(triangle_ll_3D.taxels[:,0:3]-triangle_ll_3D.pos,T2[0:3,0:3].T)+triangle_ll_3D.pos+T2[0:3,3].T
    triangle_ll_3D.pos+=T2[0:3,3]
    triangles_ll_3D.append(triangle_ll_3D)
    
    i+=1

# plot 3D taxels
# right leg
fig=plt.figure("3D triangles right")
ax=fig.add_subplot(projection="3d")

for t in triangles3D:
    taxels=t.get_taxels()
    ax.scatter(taxels[:,0],taxels[:,1],taxels[:,2])

    """
    # label taxels
    for i, txt in enumerate(taxels[:,3]):
        label=int(txt)
        ax.text(taxels[i,0], taxels[i,1], taxels[i,2], label)
    """
    
# label triangles
tr_ids=np.array([t.ID for t in triangles3D])
tr_poses=np.array([t.pos for t in triangles3D])
for i,txt in enumerate(tr_ids):
    label=int(txt)
    ax.text(tr_poses[i,0], tr_poses[i,1], tr_poses[i,2], label)
"""
# plot systems
for system in systems:
    plot_system(ax, system)
"""
set_axes_equal(ax)
#plt.show()


# left leg
fig=plt.figure("3D triangles left")
ax=fig.add_subplot(projection="3d")

for t in triangles_ll_3D:
    taxels=t.get_taxels()
    ax.scatter(taxels[:,0],taxels[:,1],taxels[:,2])

    """
    # label taxels
    for i, txt in enumerate(taxels[:,3]):
        label=int(txt)
        ax.text(taxels[i,0], taxels[i,1], taxels[i,2], label)
    """

# label triangles
tr_ids=np.array([t.ID for t in triangles_ll_3D])
tr_poses=np.array([t.pos for t in triangles_ll_3D])
for i,txt in enumerate(tr_ids):
    label=int(txt)
    ax.text(tr_poses[i,0], tr_poses[i,1], tr_poses[i,2], label)
"""
# plot systems
for system in systems2:
    plot_system(ax, system)
"""
set_axes_equal(ax)
plt.show()
"""
# load pointcloud from cascade and plot it

ptcloud=np.load("lower_right_leg1_3D.npy","r")
pts=np.copy(ptcloud)
pts=pts*1000

#T=transformation_matrix(0,0,0,-np.pi/2,0,np.pi/2)
#pts=np.dot(pts,T[0:3,0:3].T)
#ax.scatter(pts[:,0],pts[:,1],pts[:,2])

outliers=np.where((pts[:,0]>20)|(pts[:,1]>100))
for pt in outliers:
    pts=np.delete(pts,pt,0)
ax.scatter(pts[:,0],pts[:,1],pts[:,2])

set_axes_equal(ax)
plt.show()
"""

#triangleList2txtCalib(triangles3D,"right_leg_lower.txt","lower right leg","r_lower_leg")
#triangleList2txtCalib(triangles_ll_3D,"left_leg_lower.txt","lower left leg","l_lower_leg")
