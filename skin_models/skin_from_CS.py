"""
Generates skin calibrations from 3D triangle transformations (extracted from CAD) and 2D skin data.
parts: upper and lower legs, upper arms
"""
import numpy as np
from utils import homogenize, transformation_matrix, plot_system, set_axes_equal
import matplotlib.pyplot as plt
from triangle import Triangle, triangleList2txtCalib, plot3DTriangles
from read_ini import triangles2D_from_ini
from fix_lower_right_leg import get_fixed_lower_right_leg

# transformations
T_r_knee=transformation_matrix(-70.1,-529.9384739142,11.38241964656,0,-np.pi/2,-np.pi/2) # from CAD origin to right knee, retrieved from r_lower_leg_cover.stp: lines 164646-164649
T_l_knee=transformation_matrix(70.1,-529.9384739142,11.38241964655,0,-np.pi/2,-np.pi/2) # from CAD origin to left knee, retrieved from l_lower_leg_cover.stp: lines 157639-157642
#Tr_knee2urdf=transformation_matrix(0,0,0,-np.pi/2,0,0) #from knee to CS from urdf
T_r_upper_leg=transformation_matrix(-70.1,-384.1134739142,11.38241964656,0,-np.pi/2,-np.pi/2) # from CAD origin to right upper leg, retrieved from r_upper_leg_cover.stp: lines 161388-161391
T_l_upper_leg=transformation_matrix(70.1,-384.1134739142,11.38241964655,0,-np.pi/2,-np.pi/2) # from CAD origin to left upper leg, retrieved from l_upper_leg_cover.stp: lines 156819-156822

# load transformations and labels
transformations_ll=np.load("CStransformations_lll.npy")
labels_ll=np.load("CSlabels_lll.npy")
transformations_rl=np.load("CStransformations_rll.npy")
labels_rl=np.load("CSlabels_rll.npy")
transformations_lu=np.load("CStransformations_llu.npy")
labels_lu=np.load("CSlabels_llu.npy")
transformations_ru=np.load("CStransformations_rlu.npy")
labels_ru=np.load("CSlabels_rlu.npy")

transformations_la=np.load("CStransformations_la.npy")
labels_la=np.load("CSlabels_la.npy")
transformations_ra=np.load("CStransformations_ra.npy")
labels_ra=np.load("CSlabels_ra.npy")

# load triangle data from .ini file
triangles2D_left_lower=triangles2D_from_ini("left_leg_lower.ini")
triangles2D_right_lower=get_fixed_lower_right_leg()
lower_left_leg_triangles=triangles2D_left_lower[:,0]
lower_left_leg_orientations=triangles2D_left_lower[:,3]
lower_right_leg_triangles=triangles2D_right_lower[:,0]
lower_right_leg_orientations=triangles2D_right_lower[:,3]

triangles2D_left_upper=triangles2D_from_ini("left_leg_upper.ini")
triangles2D_right_upper=triangles2D_from_ini("right_leg_upper.ini")
upper_left_leg_triangles=triangles2D_left_upper[:,0]
upper_left_leg_orientations=triangles2D_left_upper[:,3]
upper_right_leg_triangles=triangles2D_right_upper[:,0]
upper_right_leg_orientations=triangles2D_right_upper[:,3]

triangles2D_left_arm=triangles2D_from_ini("left_arm_V2_7.ini")
left_arm_triangles=triangles2D_left_arm[:,0]
left_arm_orientations=triangles2D_left_arm[:,3]
triangles2D_right_arm=triangles2D_from_ini("right_arm_V2_7.ini")
right_arm_triangles=triangles2D_right_arm[:,0]
right_arm_orientations=triangles2D_right_arm[:,3]

triangles_lll_3D=[] # lower left leg triangle list
triangles_rll_3D=[] # lower right leg triangle list
triangles_llu_3D=[] # upper left leg triangle list
triangles_rlu_3D=[] # upper right leg triangle list

triangles_la_3D=[] # left arm triangle list
triangles_ra_3D=[] # right arm triangle list

# create triangles
i=0
for ID in lower_left_leg_triangles:
    index=np.where(labels_ll==ID)[0][0]
    T=transformations_ll[index]
    T=np.linalg.inv(T_l_knee) @ T #to left knee CS
    #T=np.dot(np.linalg.inv(np.dot(T_l_knee,Tr_knee2urdf)),T) #to CS from urdf
    triangle=Triangle(ID,np.zeros((3,)),lower_left_leg_orientations[i])
    triangle.taxels[:,0:3]=np.dot(triangle.taxels[:,0:3]-triangle.pos,T[0:3,0:3].T)+triangle.pos+T[0:3,3:4].T
    triangle.pos+=[T[0,3],T[1,3],T[2,3]]
    triangles_lll_3D.append(triangle)
    
    i+=1
i=0

for ID in lower_right_leg_triangles:
    index=np.where(labels_rl==ID)[0][0]
    T=transformations_rl[index]
    T=np.linalg.inv(T_r_knee) @ T #to right knee CS
    triangle=Triangle(ID,np.zeros((3,)),lower_right_leg_orientations[i])
    triangle.taxels[:,0:3]=np.dot(triangle.taxels[:,0:3]-triangle.pos,T[0:3,0:3].T)+triangle.pos+T[0:3,3:4].T
    triangle.pos+=[T[0,3],T[1,3],T[2,3]]
    triangles_rll_3D.append(triangle)
    
    i+=1
i=0

for ID in upper_left_leg_triangles:
    index=np.where(labels_lu==ID)[0][0]
    T=transformations_lu[index]
    T=np.linalg.inv(T_l_upper_leg) @ T #to left hip CS
    triangle=Triangle(ID,np.zeros((3,)),upper_left_leg_orientations[i]-90) #!!!Orientation offset!!!
    triangle.taxels[:,0:3]=np.dot(triangle.taxels[:,0:3]-triangle.pos,T[0:3,0:3].T)+triangle.pos+T[0:3,3:4].T
    triangle.pos+=[T[0,3],T[1,3],T[2,3]]
    triangles_llu_3D.append(triangle)
    
    i+=1
i=0

for ID in upper_right_leg_triangles:
    index=np.where(labels_ru==ID)[0][0]
    T=transformations_ru[index]
    T=np.linalg.inv(T_r_upper_leg) @ T #to right hip CS
    triangle=Triangle(ID,np.zeros((3,)),upper_right_leg_orientations[i]+90) #!!!Orientation offset!!!
    triangle.taxels[:,0:3]=np.dot(triangle.taxels[:,0:3]-triangle.pos,T[0:3,0:3].T)+triangle.pos+T[0:3,3:4].T
    triangle.pos+=[T[0,3],T[1,3],T[2,3]]
    triangles_rlu_3D.append(triangle)
    
    i+=1
i=0

for ID in left_arm_triangles:
    try:
        index=np.where(labels_la==ID)[0][0]
        T=transformations_la[index]
        # TODO: transformation to left arm frame ?
        triangle=Triangle(ID,np.zeros((3,)),left_arm_orientations[i]+90) #!!!Orientation offset!!!
        triangle.taxels[:,0:3]=np.dot(triangle.taxels[:,0:3]-triangle.pos,T[0:3,0:3].T)+triangle.pos+T[0:3,3:4].T
        triangle.pos+=[T[0,3],T[1,3],T[2,3]]
        triangles_la_3D.append(triangle)
    except:
        pass

    i+=1
i=0

for ID in right_arm_triangles:
    try:
        index=np.where(labels_ra==ID)[0][0]
        T=transformations_ra[index]
        # TODO: transformation to right arm frame ?
        triangle=Triangle(ID,np.zeros((3,)),right_arm_orientations[i]-90)#!!!Orientation offset!!!
        triangle.taxels[:,0:3]=np.dot(triangle.taxels[:,0:3]-triangle.pos,T[0:3,0:3].T)+triangle.pos+T[0:3,3:4].T
        triangle.pos+=[T[0,3],T[1,3],T[2,3]]
        triangles_ra_3D.append(triangle)
    except:
        pass

    i+=1

# plot taxels
#ax=plot3DTriangles(triangles_lll_3D,"3D triangles left lower",1,False)
#ax=plot3DTriangles(triangles_rll_3D,"3D triangles right lower",1,False)
#ax=plot3DTriangles(triangles_llu_3D,"3D triangles left upper",1,False)
#ax=plot3DTriangles(triangles_rlu_3D,"3D triangles right upper",1,False)
ax=plot3DTriangles(triangles_la_3D,"3D triangles left arm",1,False)
ax=plot3DTriangles(triangles_ra_3D,"3D triangles right arm",1,True)

"""
# add coordinate frame to the plot
plot_system(ax,np.array([[0,10,0,0],
            [0,0,10,0],
            [0,0,0,10]]))
plt.show()
"""
"""
# plot only front part of upper legs (ID<80)
fig=plt.figure("3D triangles left upper")
ax=fig.add_subplot(projection="3d")
for t in triangles_llu_3D:
    if(t.ID<80):
        taxels=t.get_taxels()
        ax.scatter(taxels[:,0],taxels[:,1],taxels[:,2])

# label triangles
tr_ids=np.array([t.ID for t in triangles_llu_3D])
tr_poses=np.array([t.pos for t in triangles_llu_3D])
for i,txt in enumerate(tr_ids):
    label=int(txt)
    if(label<80):
        ax.text(tr_poses[i,0], tr_poses[i,1], tr_poses[i,2], label)

set_axes_equal(ax)
#ax.view_init(0,90)

fig=plt.figure("3D triangles right upper")
ax=fig.add_subplot(projection="3d")
for t in triangles_rlu_3D:
    if(t.ID<80):
        taxels=t.get_taxels()
        ax.scatter(taxels[:,0],taxels[:,1],taxels[:,2])

# label triangles
tr_ids=np.array([t.ID for t in triangles_rlu_3D])
tr_poses=np.array([t.pos for t in triangles_rlu_3D])
for i,txt in enumerate(tr_ids):
    label=int(txt)
    if(label<80):
        ax.text(tr_poses[i,0], tr_poses[i,1], tr_poses[i,2], label)

set_axes_equal(ax)
#ax.view_init(0,90)

plt.show()
"""
"""
# plot only a subset of triangles from upper arms
fig=plt.figure("3D triangles right upper arm - subset x")
ax=fig.add_subplot(projection="3d")
triangles_Lower=[2,3,4,5,6]
triangles_Internal=[16,17,18,19,20,21,22,23,24,29,28]
triangles_Upper=[61,56,57,58,59,53,63,48]
triangles_External=[38,41,42,43,46,47,32,33,44,45,40]
for t in triangles_ra_3D:
    if(t.ID in triangles_Upper or t.ID in triangles_Internal):
        taxels=t.get_taxels()
        ax.scatter(taxels[:,0],taxels[:,1],taxels[:,2])
        
        # label taxels
        for i, txt in enumerate(taxels[:,3]):
            label=int(txt)
            ax.text(taxels[i,0], taxels[i,1], taxels[i,2], label)

# label triangles
tr_ids=np.array([t.ID for t in triangles_ra_3D])
tr_poses=np.array([t.pos for t in triangles_ra_3D])
for i,txt in enumerate(tr_ids):
    label=int(txt)
    if(label<80):
        ax.text(tr_poses[i,0], tr_poses[i,1], tr_poses[i,2], label)

set_axes_equal(ax)
ax.view_init(90,-90) #(-90,90) for lower&external, (90,-90) for upper&internal
plt.show()

fig=plt.figure("3D triangles left upper arm - subset x")
ax=fig.add_subplot(projection="3d")
triangles_Lower=[2,3,4,5,6]
triangles_Internal=[32,33,34,35,36,37,38,39,44,45,40]
triangles_Upper=[61,56,57,58,59,53,63,48]
triangles_External=[16,17,28,29,24,25,22,26,27,30,31]
for t in triangles_la_3D:
    if(t.ID in triangles_Upper or t.ID in triangles_External):
        taxels=t.get_taxels()
        ax.scatter(taxels[:,0],taxels[:,1],taxels[:,2])
        
        # label taxels
        for i, txt in enumerate(taxels[:,3]):
            label=int(txt)
            ax.text(taxels[i,0], taxels[i,1], taxels[i,2], label)

# label triangles
tr_ids=np.array([t.ID for t in triangles_ra_3D])
tr_poses=np.array([t.pos for t in triangles_ra_3D])
for i,txt in enumerate(tr_ids):
    label=int(txt)
    if(label<80):
        ax.text(tr_poses[i,0], tr_poses[i,1], tr_poses[i,2], label)

set_axes_equal(ax)
ax.view_init(90,90) #(-90,-90) for lower&internal, (90,90) for upper&internal
plt.show()
"""
# create calibration text files
#triangleList2txtCalib(triangles_rll_3D,"calibrations/right_leg_lower.txt","r_lower_leg",4)
#triangleList2txtCalib(triangles_lll_3D,"calibrations/left_leg_lower.txt","l_lower_leg",4)
#triangleList2txtCalib(triangles_rlu_3D,"calibrations/right_leg_upper.txt","r_upper_leg",7)
#triangleList2txtCalib(triangles_llu_3D,"calibrations/left_leg_upper.txt","l_upper_leg",7)
triangleList2txtCalib(triangles_la_3D,"calibrations/left_arm.txt","l_upper_arm",4)
triangleList2txtCalib(triangles_ra_3D,"calibrations/right_arm.txt","r_upper_arm",4)
