import numpy as np
from triangle import Triangle, triangleList2txtCalib, plot3DTriangles
from read_ini import triangles2D_from_ini
import matplotlib.pyplot as plt
from utils import rot_matrix, set_axes_equal, plot_system, transformation_matrix, homogenize
from fix_lower_right_leg import get_fixed_lower_right_leg

# extract position dictionary from urdf
with open("model.urdf", "r") as f:
    data = f.read()
i = 0
triangles = {}
while True:
    link = data.find("skin", i)
    if "child" in data[link-50:link]:
        i = link+1
        continue
    if link == -1:
        break
    name_start = data.find("link name=", link-50)
    name_end = data.find('"/>', name_start)
    name = data[name_start+11:name_end]
    origin_start = data.find("origin xyz", link)
    origin_end = data.find('"/>', origin_start)
    coords = data[origin_start+12:origin_end]
    coords = list(map(float, coords.replace('" rpy="', ' ').split(" ")))
    triangles[name] = coords
    i = origin_end

# load triangle data from .ini files
triangles2D_l_sole=triangles2D_from_ini("left_foot.ini")
triangles2D_r_sole=triangles2D_from_ini("right_foot.ini")
l_sole_triangles=triangles2D_l_sole[:,0]
l_sole_orientations=triangles2D_l_sole[:,3]
r_sole_triangles=triangles2D_r_sole[:,0]
r_sole_orientations=triangles2D_r_sole[:,3]

triangles3D_ls=[]
triangles3D_rs=[]

# create triangles
i=0
for ID in l_sole_triangles:
    coords=triangles["l_sole_skin_"+str(ID)]
    triangle=Triangle(ID,np.array([coords[0]*1000,coords[1]*1000,coords[2]*1000]),l_sole_orientations[i]+90-15) #!!!orientation offset!!!
    triangle.rotate_rpy(0,np.pi,0)
    triangles3D_ls.append(triangle)

    i+=1
i=0

for ID in r_sole_triangles:
    coords=triangles["r_sole_skin_"+str(ID)]
    triangle=Triangle(ID,np.array([coords[0]*1000,coords[1]*1000,coords[2]*1000]),r_sole_orientations[i]+90+15) #!!!orientation offset!!!
    triangle.rotate_rpy(0,np.pi,0)
    triangles3D_rs.append(triangle)

    i+=1

# plot taxels
ax=plot3DTriangles(triangles3D_ls,"3D triangles left sole",1,False)
ax.view_init(90,-90) #90,-90 = bottom view, -90,-90 = top view

ax=plot3DTriangles(triangles3D_rs,"3D triangles right sole",1,False)
ax.view_init(90,-90) #90,-90 = bottom view, -90,-90 = top view

plt.show()

# create calibration text files
#triangleList2txtCalib(triangles3D_rs,"calibrations/right_foot.txt","r_foot",2)
#triangleList2txtCalib(triangles3D_ls,"calibrations/left_foot.txt","l_foot",2)